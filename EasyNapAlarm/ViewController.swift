//
//  ViewController.swift
//  EasyNapAlarm
//
//  Created by John Fontana on 10/8/18.
//  Copyright © 2018 John Fontana. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    let reuseIdentifier = "cell"
    var items = ["10 min", "15 min","20 min", "30 min","45 min","60 min"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! TimeCollectionViewCell
        //cell.backgroundColor = UIColor.black
        
        //cell.TimeLabel.textColor = UIColor.white
        cell.TimeLabel.text = self.items[indexPath.item]
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "bg.jpg")?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }


}

