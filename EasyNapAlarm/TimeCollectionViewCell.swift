//
//  TimeCollectionViewCell.swift
//  EasyNapAlarm
//
//  Created by John Fontana on 10/8/18.
//  Copyright © 2018 John Fontana. All rights reserved.
//

import UIKit

class TimeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var TimeLabel: UILabel!
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                self.contentView.backgroundColor = UIColor.red
            }
            else
            {
                self.transform = CGAffineTransform.identity
                self.contentView.backgroundColor = UIColor.gray
            }
        }
    }
}
